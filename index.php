<?php
    require_once('classes/data.php');
    require_once('classes/helper.php');
    
        
    if(@$_POST)
    {
        //get selected products id
        $selectedIDArray = @$_POST['products'];
        
        if(is_array($selectedIDArray) && count($selectedIDArray) > 0)
        {
            //get selected products array
            $selectProductArray = Data::getProductsByID($selectedIDArray);
                        
            //package products
            require_once 'classes/process.php';
            $processObj = new Process();
            
            $packageArray = $processObj->getPackages($selectProductArray);
            
            if(is_array(@$packageArray) && count($packageArray) > 0)
            {
                //prepare display
                $alertMessage = 'This order has following packages:<br/>';
                foreach($packageArray as $key => $package)
                {
                    //prepare products display -- e.g: items 1, item 2,..
                    $productsString = '';
                    $tempCount = 0;
                    $selectedProductsCount = count($package['selectedProducts']);
                    foreach($package['selectedProducts'] as $product)
                    {
                        $delimeter = ($tempCount < $selectedProductsCount-1) ? ', ' : '';
                        $tempCount++; 
                        
                        $productsString .= "Item $product[id] $delimeter";
                    }
                    
                    //get courier charges for package
                    $totalCourierCharges = Data::getCourierCharges($package['totalWeight']);
                    $totalCourierCharges = ($totalCourierCharges === false) ? 'Undefined' : '$'.Helper::formatNumberForPrice($totalCourierCharges);
                    
                    //prepare order content display
                    $alertMessage .= "<b>Package".($key+1)."</b><br/>
                                        Items - $productsString<br/>
                                        Total Weight -  $package[totalWeight]g<br/>
                                        Total Price - $".Helper::formatNumberForPrice($package['totalPrice'])."<br/>
                                        Courier Price - $totalCourierCharges<br/><br/>";
                }                
                $alert = Helper::getAlert($alertMessage, 'success', false);   
            }
            else
            {
                $alert = Helper::getAlert('Sorry, something went wrong.', 'danger');
            }            
        }
        else
        {
            $alert = Helper::getAlert('Please select product(s).', 'danger');
        }
    }
    
    
    $productsArray = Data::getProducts();
?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="author" content="nader" />

	<title>Test</title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet"/>
</head>

<body>
    
    <div class="container">
    
        <?php echo @$alert; ?>
        
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
            <input type="submit" class="btn btn-lg btn-primary pull-right" value="Place Order" />    
            <div class="clr height-20"></div>        
            <div class="row">
            <?php
                //display products
                if(is_array(@$productsArray) && count($productsArray)>0)
                {
                    foreach($productsArray as $product)
                    {
                        ?>
                        <div class="col-md-3 product">
                            <div>
                                <h3><input type="checkbox" name="products[]" value="<?php echo $product['id']?>"/>&nbsp;Item <?php echo $product['id']; ?></h3>
                                Price: $<?php echo Helper::formatNumberForPrice($product['price']); ?><br />
                                Weight: <?php echo $product['weight']; ?>g 
                            </div>            
                        </div>  
                        <?php
                    }
                }
                else
                {
                    echo 'Sorry, no products available at the moment.';
                }
            ?>
            </div>                
            <div class="height-10"></div>     
            <input type="submit" class="btn btn-lg btn-primary pull-right" value="Place Order" />
            <div class="clr"></div>
            <input type="hidden" name="action"/>
        </form>        
    </div>
    
   
    <script src="js/jquery-1.11.3.min.js"></script>    
    <script src="js/bootstrap.min.js"></script>
</body>
</html>