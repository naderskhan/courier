<?php

Class Process{
    
    /**
    * Distributes products into package(s)
    * Returns Array
    */
    public function getPackages($productArray)
    {   
        $packageArray = []; //return array
        
        //get total price for selected products --if more than max package price, split packages        
        $selectedProductsPrice = $productArray['totalPrice'];
        $maxPackagePrice = Data::getMaxPackagePrice();
                
        if($selectedProductsPrice <= $maxPackagePrice)
        {
            $packageArray[] = ['totalPrice' => $selectedProductsPrice,
                                'totalWeight' => $productArray['totalWeight'],
                                'selectedProducts' => $productArray['products']];
        }
        else
        {
            //get number of packages required
            $numOfPackages = ceil($selectedProductsPrice / $maxPackagePrice);
            
            //initiate package array with 0 values
            for($i=0; $i < $numOfPackages; $i++)
            {
                $packageArray[] = ['totalWeight' => 0,
                                    'totalPrice' => 0,
                                    'selectedProducts' => []];
            }
            
            //sort products by weight
            usort($productArray['products'], function($a, $b){ return $a['weight'] > $b['weight']; });
                    
            //loop through products and push into packages            
            while($product = array_pop($productArray['products'])) 
            {
                //get lightest package index
                $index = self::getLightestPackageIndex($packageArray);
                                                              
                //if package exceeded max package price, push product to next package
                if($packageArray[$index]['totalPrice'] + $product['price'] > $maxPackagePrice) 
                {
                    //get next lightest package index 
                    $index = self::getLightestPackageIndex($packageArray, true);
                    
                    //skip product if can't fit into package
                    if ($packageArray[$index]['totalPrice'] + $product['price'] > $maxPackagePrice)
                    {
                        continue;  
                    } 
                }
                
                //push products into packages -- increment weight/price
                $packageArray[$index]['selectedProducts'][] = $product;
                $packageArray[$index]['totalWeight'] += $product['weight'];
                $packageArray[$index]['totalPrice'] += $product['price'];
            }
        }
        
        return $packageArray;
    }
    
    
    /**
    * Get index of the lightest package
    * Returns Integer
    */
    public function getLightestPackageIndex($packageArray, $getNextLightestPackage = false)
    {
        if(is_array(@$packageArray) && count($packageArray) > 0)
        {
            $tempWeightArray = [];
            foreach($packageArray as $index => $package)
            {
                $tempWeightArray[$index] = $package['totalWeight'];                
            }
            
            //get next lightest package index
            if($getNextLightestPackage)
            {
                unset($tempWeightArray[array_search(min($tempWeightArray), $tempWeightArray)]);
            }
            
            return array_search(min($tempWeightArray), $tempWeightArray);
        }
    }
    
}