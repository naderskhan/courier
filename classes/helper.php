<?php

Class Helper{
    
    /**
    * $type = success | info | warning | danger
    * returns string
    */
    public static function getAlert($message, $type = "success", $hasDismiss = true)
    {
        return "<div class='alert alert-{$type} ".($hasDismiss ? 'alert-dismissable' : '')."'>
                    {$message}
                    ".($hasDismiss ? "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>x</button>" : "")."
                </div>";
    }
    
    public static function printArray($array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }
    
    public static function debug()
    {
        ini_set('display_errors',1);
        ini_set('display_startup_errors',1);
        error_reporting(-1);        
    }
    
    public static function formatNumberForPrice($amount)
    {
        return number_format($amount, 2, ".", ",");
    }
        
    public static function cleanVarForInt($inputVar)
    {
        $inputVar = @trim($inputVar);
        $inputVar = str_replace(" ", "", $inputVar);
        $inputVar = str_replace(",", "", $inputVar);
        $inputVar = intval($inputVar);
        return $inputVar;
    }    
}