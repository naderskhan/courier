<?php

/**
* This class acts as the data source for this project
* In a real project, should be replaced by a database instead
*/
Class Data{
    
    public static function getProducts()
    {
        return [    1 => ['id' => 1,
                            'price' => 10,
                            'weight' => 200],
                    2 => ['id' => 2,
                            'price' => 100,
                            'weight' => 20],  
                    3 => ['id' => 3,
                            'price' => 30,
                            'weight' => 300],
                    4 => ['id' => 4,
                            'price' => 20,
                            'weight' => 500],
                    5 => ['id' => 5,
                            'price' => 30,
                            'weight' => 250],
                    6 => ['id' => 6,
                            'price' => 40,
                            'weight' => 10],
                    7 => ['id' => 7,
                            'price' => 200,
                            'weight' => 10],  
                    8 => ['id' => 8,
                            'price' => 120,
                            'weight' => 500],
                    9 => ['id' => 9,
                            'price' => 130,
                            'weight' => 790],
                    10 => ['id' => 10,
                            'price' => 20,
                            'weight' => 100], 
                    11 => ['id' => 11,
                            'price' => 10,
                            'weight' => 340],
                    12 => ['id' => 12,
                            'price' => 4,
                            'weight' => 800],  
                    13 => ['id' => 13,
                            'price' => 5,
                            'weight' => 200],
                    14 => ['id' => 14,
                            'price' => 240,
                            'weight' => 20],
                    15 => ['id' => 15,
                            'price' => 123,
                            'weight' => 700],
                    16 => ['id' => 16,
                            'price' => 245,
                            'weight' => 10],
                    17 => ['id' => 17,
                            'price' => 230,
                            'weight' => 20],  
                    18 => ['id' => 18,
                            'price' => 110,
                            'weight' => 200],
                    19 => ['id' => 19,
                            'price' => 45,
                            'weight' => 200],
                    20 => ['id' => 20,
                            'price' => 67,
                            'weight' => 20],
                    21 => ['id' => 21,
                            'price' => 88,
                            'weight' => 300],
                    22 => ['id' => 22,
                            'price' => 10,
                            'weight' => 500],  
                    23 => ['id' => 23,
                            'price' => 17,
                            'weight' => 250],
                    24 => ['id' => 24,
                            'price' => 19,
                            'weight' => 10],
                    25 => ['id' => 25,
                            'price' => 89,
                            'weight' => 10],
                    26 => ['id' => 26,
                            'price' => 45,
                            'weight' => 500],
                    27 => ['id' => 27,
                            'price' => 99,
                            'weight' => 790],  
                    28 => ['id' => 28,
                            'price' => 125,
                            'weight' => 100],
                    29 => ['id' => 29,
                            'price' => 198,
                            'weight' => 340],
                    30 => ['id' => 30,
                            'price' => 220,
                            'weight' => 800],
                    31 => ['id' => 31,
                            'price' => 249,
                            'weight' => 200],
                    32 => ['id' => 32,
                            'price' => 230,
                            'weight' => 20],  
                    33 => ['id' => 33,
                            'price' => 190,
                            'weight' => 700],
                    34 => ['id' => 34,
                            'price' => 45,
                            'weight' => 10],
                    35 => ['id' => 35,
                            'price' => 12,
                            'weight' => 20],
                    36 => ['id' => 36,
                            'price' => 5,
                            'weight' => 200],
                    37 => ['id' => 37,
                            'price' => 2,
                            'weight' => 200],  
                    38 => ['id' => 38,
                            'price' => 90,
                            'weight' => 20],
                    39 => ['id' => 39,
                            'price' => 12,
                            'weight' => 300],
                    40 => ['id' => 40,
                            'price' => 167,
                            'weight' => 500],                             
                    41 => ['id' => 41,
                            'price' => 12,
                            'weight' => 250],
                    42 => ['id' => 42,
                            'price' => 8,
                            'weight' => 10],  
                    43 => ['id' => 43,
                            'price' => 2,
                            'weight' => 10],
                    44 => ['id' => 44,
                            'price' => 9,
                            'weight' => 500],
                    45 => ['id' => 45,
                            'price' => 210,
                            'weight' => 790],
                    46 => ['id' => 46,
                            'price' => 167,
                            'weight' => 100],
                    47 => ['id' => 47,
                            'price' => 23,
                            'weight' => 340],  
                    48 => ['id' => 48,
                            'price' => 190,
                            'weight' => 800],
                    49 => ['id' => 49,
                            'price' => 199,
                            'weight' => 200],
                    50 => ['id' => 50,
                            'price' => 12,
                            'weight' => 20]
                ];
    }
    
    public static function getCourierCharges($weight)
    {
        if($weight >= 0 && $weight <= 200)
        {
            return 5;
        }
        else if($weight >= 201 && $weight <= 500)
        {
            return 10;
        }
        else if($weight >= 501 && $weight <= 1000)
        {
            return 15;
        }
        else if($weight >= 1001 && $weight <= 5000)
        {
            return 20;
        }
        else
        {
            return false;
        }
    }
    
        
    public static function getProductsByID($selectIDArray)
    {
        $allProductsArray = self::getProducts();
        
        $selectedProductsArray = [];
        $totalPrice = 0;
        $totalWeight = 0;
        
        if(is_array(@$selectIDArray) && count($selectIDArray) > 0)
        {
            foreach($selectIDArray as $productID)
            {
                $selectedProductsArray['products'][$productID] = $allProductsArray[$productID];   
                $totalPrice += $allProductsArray[$productID]['price'];
                $totalWeight += $allProductsArray[$productID]['weight'];
            }
            
            $selectedProductsArray['totalPrice'] = $totalPrice;
            $selectedProductsArray['totalWeight'] = $totalWeight;
        }
        
        return $selectedProductsArray;
    }
    
    
    public static function getMaxPackagePrice()
    {
        return 250;
    }
    
}